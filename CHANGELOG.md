# 0.5.0 > 0.8.0

When loading service in moleculer context

```js
const DBMiddleware = require('@voilab/vmol-objection');

// becomes

const { DBMiddleware } = require('@voilab/vmol-objection');
```

When loading model

```js
const { Model } = require('objection');
class MyModel extends Model {}

// becomes

const { BaseModel } = require('@voilab/vmol-objection');
class MyModel extends BaseModel {}
```

In `package.json`, load new dependancies. Node 14 is the minimum version for objection 3.1. By the way
AJV formats is loaded by default since version 3.0.5.

 ```json
 {
    "engines": {
        "node": ">=14.0.0"
    },
    "dependancies": {
        "@voilab/vmol-objection": "^0.7.1",
        "knex": "^2.0.0",
        "objection": "^3.0.1",
        "ajv": "^8.11.0",
        "ajv-keywords": "^5.1.1"
    }
}
```

Differences in defining types and formats in Model file, `get jsonSchema` function

```json
{
    "data": { "type": "jsonb" },
    "becomes",
    "data": { "type": "object" }
}
{
    "data": { "type": "date" },
    "becomes",
    "data": { "instanceof": "Date" }
}
```
