'use strict';

module.exports = {
    up(knex) {
        return knex.schema
            .createTable('test', table => {
                table.increments('id');
                table.string('foo', 128).notNullable();
                table.string('bar', 128).notNullable();
            });
    },

    down(knex) {
        return knex.schema
            .dropTable('test');
    }
};
