'use strict';

const { Model, AjvValidator } = require('objection');

let addKeywords;

try {
    addKeywords = require('ajv-keywords');
    // eslint-disable-next-line
} catch (e) {}

class BaseModel extends Model {
    static createValidator() {
        return new AjvValidator({
            onCreateAjv: (ajv) => {
                addKeywords && addKeywords(ajv);
            },
            options: {
                allErrors: true,
                validateSchema: false,
                ownProperties: true,
                v5: true,
                strict: false
            }
        });
    }
}

module.exports = BaseModel;
