'use strict';

const lodash = require('lodash');
const promiseRetry = require('promise-retry');

const { Model } = require('objection');

const migrationsDone = {};

/**
 * Service mixin to access database entities using Objection.js
 *
 * @name moleculer-db-objection
 * @module Service
 */
module.exports = {
    /**
     * Default settings
     */
    settings: {
        /**
         * @type {Boolean} Enable auto schema migration on service start.
         */
        autoMigrate: false,

        /**
         * @type {Array} Hide fields from the responses.
         */
        secureFields: []
    },

    /**
     * @type {Array} List of model classes managed.
     * @type {Object} Indexed List of model classes managed.
     */
    models: [],

    /**
     * @type {Object} Knex instance.
     */
    knex: null,

    /**
     * Methods
     */
    methods: {
        getMigrationTableName(model) {
            return lodash.get(
                model.knex().client.config,
                'migrations.tableName',
                'knex_migrations'
            );
        },

        /**
         * Check the migration table name and run migrations only once
         */
        async migrateDatabase() {
            return this.runOnAllModels(model => {
                const migrationTable = this.getMigrationTableName(model);

                if (!migrationsDone[migrationTable]) {
                    migrationsDone[migrationTable] = true;
                    return this.runMigration(model);
                }
            });
        },

        /**
         * Run migrations for one model
         */
        async runMigration(model) {
            return promiseRetry(
                (retry) => {
                    this.logger.debug(`Running migrations on model ${model.name} for service ${this.name}`);
                    return model.knex().migrate.latest()
                        .then(res => {
                            this.logger.info(`Migrations successful for model ${model.name}`);
                            return res;
                        })
                        .catch(e => {
                            this.logger.error(`Migrations for model ${model.name} failed with error: ${e.message}`);
                            return retry();
                        });
                },
                {
                    retries: 5
                }
            );
        },

        /**
         * Remove secure fields form an object or a collection of objects.
         *
         * @param {Object|Array<Object>} collection - Objects to filter
         * @param {String|Array<String>} keepFields - Fields to be kept, disregarding secureFields
         *
         * @returns {Object|Array<Object>} Filtered collection.
         */
        filterSecureFields(collection, keepFields = []) {
            if (!Array.isArray(this.settings.secureFields) || !this.settings.secureFields.length) {
                return collection;
            }

            keepFields = !Array.isArray(keepFields) ? [keepFields] : keepFields;

            const omitFields = lodash.difference(this.settings.secureFields, keepFields);

            if (Array.isArray(collection)) {
                return lodash.map(collection, r => lodash.omit(r, omitFields));
            } else {
                return lodash.omit(collection, omitFields);
            }
        },

        /**
         * Helper to run a function on all registred models and then wait for the resulting promises.
         *
         * @param {Function} func
         * @returns {Promise}
         */
        runOnAllModels(func) {
            const promises = [];

            for (const model in this.models) {
                promises.push(func(this.models[model]));
            }

            return Promise.all(promises);
        }
    },

    /**
     * Service created lifecycle event handler
     */
    created() {
        if ((Array.isArray(this.schema.models) && !this.schema.models.length) ||
            (typeof this.schema.models == 'object' && !Object.keys(this.schema.models).length) ||
            typeof this.schema.models != 'object') {
            throw new Error('`models` must be either an array or an object of model instances!');
        }

        if (typeof this.schema.knex !== 'function' || this.schema.knex.name !== 'knex') {
            throw new Error('`knex` must be a valid Knex instance!');
        }

        this.models = {};

        if (Array.isArray(this.schema.models)) {
            this.schema.models.forEach(model => {
                this.models[model.name] = model;
            });
        } else {
            this.models = this.schema.models;
        }

        for (const model in this.models) {
            if (!(this.models[model].prototype instanceof Model)) {
                throw new Error('model must be a valid Objection Model!');
            }
        }

        this.knex = this.schema.knex;
    },

    /**
     * Service started lifecycle event handler
     */
    async started() {
        await this.runOnAllModels(model => {
            this.logger.debug(`Injecting knex instance to model: ${model.name}`);
            model.knex(this.knex);
            return true;
        });

        if (this.settings.autoMigrate) {
            return this.migrateDatabase();
        }
    },

    /**
     * Service stopped lifecycle event handler
     */
    async stopped() {
        return this.runOnAllModels(model => {
            return model.knex().destroy();
        });
    },
};
