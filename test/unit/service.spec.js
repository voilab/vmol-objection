'use strict';

const { ServiceBroker } = require('moleculer');
const lodash = require('lodash');
const Knex = require('knex');
const { BaseModel, DBMiddleware } = require('../../index');

const { QueryBuilder } = require('objection');

class MockQueryBuilder extends QueryBuilder {
    query() {
        return super.query().resolve();
    }
}

class MockModel extends BaseModel {
    static get tableName() {
        return 'base_model';
    }

    static get QueryBuilder() {
        return MockQueryBuilder;
    }
}

describe('Test DBMiddleware creation', () => {
    const broker = new ServiceBroker({ logger: false, validation: false });
    const knexFile = {
        client: 'sqlite',
        connection: {
            filename: 'testdb.sqlite'
        },
        useNullAsDefault: true
    };

    const baseService = {
        name: 'objection',
        models: [MockModel],
        knex: Knex(knexFile)
    };

    it('should throw when models is not set', async () => {
        function createService() {
            broker.createService(DBMiddleware, lodash.omit(baseService, ['models']));
        }

        expect(createService).toThrowError('`models` must be either an array or an object of model instances!');
    });

    it('should throw when knex is not set', async () => {
        function createService() {
            broker.createService(DBMiddleware, lodash.omit(baseService, ['knex']));
        }

        expect(createService).toThrowError('must be a valid Knex instance!');
    });

    it('should should throw on invalid model', async () => {
        function createService() {
            broker.createService(DBMiddleware, {
                name: 'objection',
                models: [{}],
                knex: Knex(knexFile)
            });
        }

        expect(createService).toThrowError('must be a valid Objection Model!');
    });

    it('should set knex instance on models', async () => {
        const localKnex = Knex(knexFile);

        const service = broker.createService(DBMiddleware, {
            name: 'objection',
            models: [MockModel],
            knex: localKnex
        });

        await broker.start();
        expect(MockModel.knex()).toBe(localKnex);
        await broker.stop();
        await broker.destroyService(service);
    });

    it('should NOT run migrations by default', async () => {
        const spie = jest.spyOn(DBMiddleware.methods, 'migrateDatabase')
            .mockImplementation(jest.fn(() => {}));
        const service = broker.createService(DBMiddleware, baseService);

        await broker.start();
        expect(spie).toHaveBeenCalledTimes(0);
        await broker.stop();
        await broker.destroyService(service);
        spie.mockRestore();
    });

    it('should use the default migration table name when none is provided', async () => {
        const service = broker.createService(DBMiddleware, baseService);

        await broker.start();
        expect(service.getMigrationTableName(service.models.MockModel)).toBe('knex_migrations');
        await broker.stop();
        await broker.destroyService(service);
    });

    it('should use custom migration table name when provided', async () => {
        knexFile.migrations = {
            tableName: 'knex_migrations_custom'
        };

        const service = broker.createService(DBMiddleware, {
            name: 'objection',
            models: [MockModel],
            knex: Knex(knexFile)
        });

        await broker.start();
        expect(service.getMigrationTableName(service.models.MockModel)).toBe('knex_migrations_custom');
        await broker.stop();
        await broker.destroyService(service);
    });

    it('should run migrations only once', async () => {
        const spie = jest.spyOn(DBMiddleware.methods, 'runMigration')
            .mockImplementation(jest.fn(() => {}));

        knexFile.migrations = {
            tableName: 'knex_migrations_count'
        };

        const service = broker.createService(DBMiddleware, {
            name: 'objection',
            models: [MockModel],
            knex: Knex(knexFile),
            settings: {
                autoMigrate: true
            }
        });

        await broker.start();
        expect(spie).toHaveBeenCalledTimes(1);
        await broker.stop();
        await broker.start();
        expect(spie).toHaveBeenCalledTimes(1);
        await broker.stop();
        await broker.destroyService(service);

        spie.mockRestore();
    });
});

describe('Secure fields filtering', () => {
    const broker = new ServiceBroker({ logger: false, validation: false });
    const knexFile = {
        client: 'sqlite',
        connection: {
            filename: 'testdb.sqlite'
        },
        useNullAsDefault: true
    };

    const knex = Knex(knexFile);

    const baseService = {
        name: 'objection',
        models: [MockModel],
        knex
    };

    const mockObject = {
        foo: 'bar',
        bar: 'foo'
    };

    const mockCollection = [mockObject];

    const service = broker.createService(DBMiddleware, baseService);

    beforeAll(() => broker.start());
    afterAll(() => broker.stop());

    it('should not modify collection when `secureFields` is not defined', () => {
        delete service.settings.secureFields;

        expect(service.filterSecureFields(mockObject))
            .toEqual(mockObject);
        expect(service.filterSecureFields(mockCollection))
            .toEqual(mockCollection);
    });

    it('should remove fields based on `secureFields`', () => {
        service.settings.secureFields = ['foo'];

        expect(service.filterSecureFields(mockObject))
            .toEqual({bar: 'foo'});
        expect(service.filterSecureFields(mockCollection))
            .toEqual([{bar: 'foo'}]);
    });

    it('should preserve `keepFields` present in `secureFields`', () => {
        service.settings.secureFields = ['foo'];

        expect(service.filterSecureFields(mockObject, ['foo']))
            .toEqual(mockObject);
        expect(service.filterSecureFields(mockCollection, ['foo']))
            .toEqual(mockCollection);
    });
});

describe('Database interaction', () => {
    const broker = new ServiceBroker({ logger: false, validation: false });
    const knexFile = {
        client: 'sqlite',
        connection: {
            filename: 'testdb.sqlite'
        },
        useNullAsDefault: true
    };

    const knex = Knex(knexFile);

    const baseService = {
        name: 'objection',
        models: [MockModel],
        knex
    };

    const service = broker.createService(DBMiddleware, baseService);

    beforeAll(async () => {
        await knex.raw('drop table if exists knex_migrations;');
        await knex.raw('drop table if exists knex_migrations_lock;');
        await knex.raw('drop table if exists test;');
        await broker.start();
    });
    afterAll(() => broker.stop());

    it('`should apply migrations', async () => {
        await service.migrateDatabase();
        const result = await service.knex.raw('select * from test;');
        expect(result).toEqual([]);
    });
});
