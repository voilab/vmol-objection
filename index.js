'use strict';

const DBMiddleware = require('./src/DBMiddleware');
const BaseModel = require('./src/BaseModel');

module.exports = {
    DBMiddleware,
    BaseModel
};
